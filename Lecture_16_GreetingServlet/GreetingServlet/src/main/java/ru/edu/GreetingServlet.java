package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GreetingServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Person person = Person.builder()
                .setFirstName("Zakhar")
                .setLastName("Borisov")
                .setSecondName("Andreevich")
                .setPhone("89841087493")
                .setHobby("Ski, Java programming")
                .setBitBucketUrl("https://bitbucket.org/ascjke/sber_edu")
                .build();

        req.setAttribute("firstName", person.getFirstName());
        req.setAttribute("lastName", person.getLastName());
        req.setAttribute("secondName", person.getSecondName());
        req.setAttribute("phone", person.getPhone());
        req.setAttribute("hobby", person.getHobby());
        req.setAttribute("bitBucketUrl", person.getBitBucketUrl());

        getServletContext().getRequestDispatcher("/author.jsp").forward(req, resp);

//        resp.setCharacterEncoding("UTF-8");
//        PrintWriter writer = resp.getWriter();
//        writer.write("<html>");
//        writer.write("<head>");
//        writer.write("<meta charset=\"UTF-8\">");
//        writer.write("</head>");
//        writer.write("<body>");
//        writer.write("<h1>Информация об авторе</h1>");
//        writer.write("Фамилия: " + person.getLastName() + "<br>");
//        writer.write("Имя: " + person.getFirstName() + "<br>");
//        writer.write("Отчество: " + person.getSecondName() + "<br>");
//        writer.write("Телефон: " + person.getPhone() + "<br>");
//        writer.write("Хобби: " + person.getHobby() + "<br>");
//        writer.write("BitBucketUrl: " + person.getBitBucketUrl());
//        writer.write("</body>");
//        writer.write("</html>");
    }
}
