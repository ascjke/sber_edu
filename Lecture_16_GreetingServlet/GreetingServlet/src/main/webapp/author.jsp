<%@ page isELIgnored="false"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<html>

    <body>
        <h1>Информация об авторе</h1>
        Фамилия: ${lastName} <br>
        Имя: ${firstName} <br>
        Отчество: ${secondName} <br>
        Телефон: ${phone} <br>
        Хобби: ${hobby} <br>
        BitBucketUrl: ${bitBucketUrl}
    </body>

</html>