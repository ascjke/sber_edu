<%@ page isELIgnored="false"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<html>
    <body>
        <h1>Калькулятор доходности вклада</h1>
        <form method="POST" action="">
            <p><big>Сумма на момент открытия: <input name="sum"></big></p>
            <p><big>Процентная ставка: <input name="percentage"></big></p>
            <p><big>Количество лет <input name="years"></big></p>
            <input type="submit" value="Посчитать">
        </form>
    </body>
</html>