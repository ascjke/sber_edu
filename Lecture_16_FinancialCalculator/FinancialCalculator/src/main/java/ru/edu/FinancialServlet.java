package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FinancialServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/finance.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String sum = req.getParameter("sum");
        String percentage = req.getParameter("percentage");
        String years = req.getParameter("years");

        boolean checkInput = !isNumericDouble(sum) ||
                            !isNumericDouble(percentage) ||
                            !isNumericInt(years);

        if (checkInput) {
            getServletContext().getRequestDispatcher("/error_2.jsp").forward(req, resp);
        } else if (Double.parseDouble(sum) < 50000 && Double.parseDouble(sum) >= 0) {
            getServletContext().getRequestDispatcher("/error_1.jsp").forward(req, resp);
        }

        Deposit deposit = new Deposit(Double.parseDouble(sum), Double.parseDouble(percentage), Integer.parseInt(years));
        String result = deposit.calculate();
        int index = result.indexOf(",");

        req.setAttribute("calculatedSumRub", result.substring(0, index));
        req.setAttribute("calculatedSumKop", result.substring(index + 1));

        getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);
    }

    public static boolean isNumericDouble(String str) {
        return str.matches("\\d+(\\.\\d+)?");
    }

    public static boolean isNumericInt(String str) {
        return str.matches("\\d+");
    }
}
