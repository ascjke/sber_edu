package ru.edu;

import java.util.Locale;

public class Deposit {

    private double sum;
    private double percentage;
    private int years;


    public Deposit(double sum, double percentage, int years) {
        this.sum = sum;
        this.percentage = percentage;
        this.years = years;
    }

    public String calculate() {

        for (int i = 0; i < years; i++) {
            sum = sum + (sum * percentage / 100.0);
        }
        sum = Math.round(sum * 100) / 100.0;

        return String.format(Locale.FRANCE, "%,.2f", sum);
    }
}
