<%@ page isELIgnored="false"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<html>
<head>
    <title>Калькулятор доходности вклада</title>
    <style>
        .customButton{
        background: #008EB0; /* Синий цвет фона */
        color: #fff; /* Белый цвет текста */
        border: none; /* Убираем рамку */
        padding: 1rem 1.5rem; /* Поля вокруг текста */
        margin-bottom: 1rem; /* Отступ снизу */
        }
    </style>
</head>
    <body>
        <h1>Калькулятор доходности вклада</h1>
        <form method="POST" action="">
            <p><big>Сумма на момент открытия: <input name="sum"></big></p>
            <p><big>Процентная ставка: <input name="percentage"></big></p>
            <p><big>Количество лет <input name="years"></big></p>
            <div id="button">
                <input type="submit" class="customButton" value="Посчитать">
            </div>
        </form>
    </body>
</html>