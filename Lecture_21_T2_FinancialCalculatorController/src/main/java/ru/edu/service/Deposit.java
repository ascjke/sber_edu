package ru.edu.service;

import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class Deposit {

    private double sum;
    private double percentage;
    private int years;

    public void setSum(double sum) {
        this.sum = sum;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public void setYears(int years) {
        this.years = years;
    }

    public String calculate() {

        for (int i = 0; i < years; i++) {
            sum = sum + (sum * percentage / 100.0);
        }
        sum = Math.round(sum * 100) / 100.0;

        return String.format(Locale.FRANCE, "%,.2f", sum);
    }
}
