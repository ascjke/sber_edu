package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.service.Deposit;

@Controller
@RequestMapping(value = "/finance")
public class FinancialCalculatorController {

    private Deposit deposit;

    @Autowired
    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    @GetMapping
    public ModelAndView getStart() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("view/finance.jsp");
        return modelAndView;
    }

    @PostMapping
    public ModelAndView calculate(@RequestParam("sum") String sum,
                                  @RequestParam("percentage") String percentage,
                                  @RequestParam("years") String years) {

        ModelAndView modelAndView = new ModelAndView();

        boolean checkInput = !isNumericDouble(sum) ||
                            !isNumericDouble(percentage) ||
                            !isNumericInt(years);

        if (checkInput) {
            modelAndView.setViewName("view/error_2.jsp");
            return modelAndView;
        } else if (Double.parseDouble(sum) < 50000 && Double.parseDouble(sum) >= 0) {
            modelAndView.setViewName("view/error_1.jsp");
            return modelAndView;
        }

        deposit.setSum(Double.parseDouble(sum));
        deposit.setPercentage(Double.parseDouble(percentage));
        deposit.setYears(Integer.parseInt(years));

        String result = deposit.calculate();
        int index = result.indexOf(",");

        modelAndView.addObject("calculatedSumRub", result.substring(0, index));
        modelAndView.addObject("calculatedSumKop", result.substring(index + 1));

        modelAndView.setViewName("view/result.jsp");
        return modelAndView;
    }

    public static boolean isNumericDouble(String str) {
        return str.matches("\\d+(\\.\\d+)?");
    }

    public static boolean isNumericInt(String str) {
        return str.matches("\\d+");
    }
}