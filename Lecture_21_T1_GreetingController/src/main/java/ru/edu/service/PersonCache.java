package ru.edu.service;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class PersonCache {

    Map<String, Person> persons = new HashMap<>();

    @PostConstruct
    public void init() {
        create(Person.builder()
                .setId("1")
                .setFirstName("Zakhar")
                .setLastName("Borisov")
                .setSecondName("Andreevich")
                .setPhone("89841087493")
                .setHobby("Ski, Java programming")
                .setBitBucketUrl("https://bitbucket.org/ascjke/sber_edu")
                .build());

        create(Person.builder()
                .setId("2")
                .setFirstName("Ivan")
                .setLastName("Alekseev")
                .setSecondName("Viktorovich")
                .setPhone("89245689874")
                .setHobby("Autotuning")
                .setBitBucketUrl("https://bitbucket.org/ivanalex/sber_edu")
                .build());

        create(Person.builder()
                .setId("3")
                .setFirstName("Elena")
                .setLastName("Nikiforova")
                .setSecondName("Borisovna")
                .setPhone("89265987423")
                .setHobby("Camping")
                .setBitBucketUrl("https://bitbucket.org/elena_888/sber_edu")
                .build());
    }

    public Person create(Person person) {
        return persons.put(person.getId(), person);
    }

    public Person update(Person person) {
        return persons.put(person.getId(), person);
    }

    public Person delete(String id) {
        return persons.remove(id);
    }

    public Person getById(String id) {
        return persons.get(id);
    }

    public Collection<Person> getAll() {
        return persons.values();
    }

}
