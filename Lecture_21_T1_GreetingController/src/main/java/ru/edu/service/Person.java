package ru.edu.service;

public class Person {

    private String id;
    private String firstName;
    private String lastName;
    private String secondName;
    private String phone;
    private String hobby;
    private String bitBucketUrl;

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPhone() {
        return phone;
    }

    public String getHobby() {
        return hobby;
    }

    public String getBitBucketUrl() {
        return bitBucketUrl;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private Person obj = new Person();

        public Builder setId(String id) {
            obj.id = id;
            return this;
        }

        public Builder setFirstName(String firstName) {
            obj.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            obj.lastName = lastName;
            return this;
        }

        public Builder setSecondName(String secondName) {
            obj.secondName = secondName;
            return this;
        }

        public Builder setPhone(String phone) {
            obj.phone = phone;
            return this;
        }

        public Builder setHobby(String hobby) {
            obj.hobby = hobby;
            return this;
        }

        public Builder setBitBucketUrl(String bitBucketUrl) {
            obj.bitBucketUrl = bitBucketUrl;
            return this;
        }

        public Person build() {
            return obj;
        }
    }
}

