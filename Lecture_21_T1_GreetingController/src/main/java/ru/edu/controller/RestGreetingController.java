package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.edu.service.Person;
import ru.edu.service.PersonCache;

import java.util.Objects;

@RestController
@RequestMapping(value = "/api/author", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class RestGreetingController {

    private PersonCache personCache;

    @Autowired
    public void setPersonCache(PersonCache personCache) {
        this.personCache = personCache;
    }

    @GetMapping
    public Person getInfoById(@RequestParam("id") String id) {
        return personCache.getById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Person create(@RequestBody Person person) {
        return personCache.create(person);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Person update(@RequestBody Person person) {
        return personCache.update(person);
    }

    @DeleteMapping("/{id}")
    public Person delete(@PathVariable("id") String id) {
        Person person = personCache.getById(id);
        if (Objects.isNull(person)) {
            throw new IllegalArgumentException("Author with id=" + id + " not found");
        }
        return personCache.delete(id);
    }
}
