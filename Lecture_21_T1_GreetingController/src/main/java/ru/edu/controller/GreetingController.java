package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.service.Person;
import ru.edu.service.PersonCache;

import java.util.Collection;

@Controller
@RequestMapping(value = "/author")
public class GreetingController {

    private PersonCache personCache;

    @Autowired
    public void setPersonCache(PersonCache personCache) {
        this.personCache = personCache;
    }

    @GetMapping
    public ModelAndView getPerson() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/view/author_default.jsp");
        Person person = personCache.getById("1");

        modelAndView.addObject("id", person.getId());
        modelAndView.addObject("lastName", person.getLastName());
        modelAndView.addObject("firstName", person.getFirstName());
        modelAndView.addObject("secondName", person.getSecondName());
        modelAndView.addObject("phone", person.getPhone());
        modelAndView.addObject("hobby", person.getHobby());
        modelAndView.addObject("bitBucketUrl", person.getBitBucketUrl());

        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView getPersonById(@PathVariable("id") String id) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/view/author.jsp");
        Person person = personCache.getById(id);

        modelAndView.addObject("id", person.getId());
        modelAndView.addObject("lastName", person.getLastName());
        modelAndView.addObject("firstName", person.getFirstName());
        modelAndView.addObject("secondName", person.getSecondName());
        modelAndView.addObject("phone", person.getPhone());
        modelAndView.addObject("hobby", person.getHobby());
        modelAndView.addObject("bitBucketUrl", person.getBitBucketUrl());
        return modelAndView;
    }

    @GetMapping("/all")
    public ModelAndView getAll() {

        ModelAndView modelAndView = new ModelAndView();
        Collection<Person> persons = personCache.getAll();

        modelAndView.setViewName("/view/all.jsp");
        modelAndView.addObject("personList", persons);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/view/create.jsp");
        return modelAndView;
    }

    @GetMapping("/index")
    public ModelAndView index() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/view/index.jsp");
        return modelAndView;
    }

    @PostMapping("/all")
    public ModelAndView createPerson(@RequestParam("id") String id, @RequestParam("lastName") String lastName,
                                     @RequestParam("firstName") String firstName, @RequestParam("secondName") String secondName,
                                     @RequestParam("phone") String phone, @RequestParam("hobby") String hobby,
                                     @RequestParam("bitBucketUrl") String bitBucketUrl) {

        personCache.create(Person.builder()
                .setId(id)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setSecondName(secondName)
                .setPhone(phone)
                .setHobby(hobby)
                .setBitBucketUrl(bitBucketUrl)
                .build());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id", id);
        modelAndView.addObject("lastName", lastName);
        modelAndView.addObject("firstName", firstName);
        modelAndView.addObject("secondName", secondName);
        modelAndView.addObject("phone", phone);
        modelAndView.addObject("hobby", hobby);
        modelAndView.addObject("bitBucketUrl", bitBucketUrl);

        return getAll();
    }

    @PostMapping("/deleteById")
    public ModelAndView delete(@RequestParam("id") String id) {

        personCache.delete(id);
        return getAll();
    }

    @GetMapping("/edit")
    public ModelAndView getEditPage(@RequestParam("id") String id) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/view/edit.jsp");
        modelAndView.addObject("author", personCache.getById(id));
        return modelAndView;
    }

    @PostMapping("/{id}")
    public ModelAndView editAuthor(@RequestParam("id") String id, @RequestParam("lastName") String lastName,
                                   @RequestParam("firstName") String firstName, @RequestParam("secondName") String secondName,
                                   @RequestParam("phone") String phone, @RequestParam("hobby") String hobby,
                                   @RequestParam("bitBucketUrl") String bitBucketUrl) {

        personCache.update(Person.builder()
                .setId(id)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setSecondName(secondName)
                .setPhone(phone)
                .setHobby(hobby)
                .setBitBucketUrl(bitBucketUrl)
                .build());

        return getPersonById(id);
    }
}