<%@ page isELIgnored="false"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<html>
<head>
    <title>Редактировать автора</title>
</head>
    <body>
        <h1>Редактировать автора</h1>
        <form method="POST" action="${author.id}">
            Id <input name="id" value="${author.id}"> <br>
            Last name: <input name="lastName" value="${author.lastName}"> <br>
            First name: <input name="firstName" value="${author.firstName}"> <br>
            Second name: <input name="secondName"value="${author.secondName}"> <br>
            Phone: <input name="phone" value="${author.phone}"> <br>
            Hobby: <input name="hobby" value="${author.hobby}"> <br>
            BitBucketUrl: <input name="bitBucketUrl" value="${author.bitBucketUrl}"> <br>
            <input type="submit" value="Редактировать">
        </form>
    </body>
</html>