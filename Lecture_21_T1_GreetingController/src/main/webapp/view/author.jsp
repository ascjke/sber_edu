<%@ page isELIgnored="false"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>Author</title>
</head>
    <body>
        <h1>Информация об авторе</h1> <br>
        <h4>Фамилия: ${lastName}</h4> <br>
        <h4>Имя: ${firstName}</h4> <br>
        <h4>Отчество: ${secondName}</h4> <br>
        <h4>Телефон: ${phone}</h4> <br>
        <h4>Хобби: ${hobby}</h4> <br>
        <h4>BitBucketUrl: ${bitBucketUrl}</h4>
        <br><br><br><br>
        <a href="/author/all">All authors</a>
        <br><br><br><br><br>
        <form method="POST" action="deleteById">
            <input hidden name="id" value="${id}">
            <input type="submit" value="Удалить">
        </form>
        <form method="GET" action="edit">
            <input hidden name="id"  value="${id}">
            <input type="submit" value="Редактировать">
        </form>
    </body>
</html>